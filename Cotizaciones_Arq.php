<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="stylesheets/estilos_clientes_adm.css" type="text/css">
  <link rel="stylesheet" href="stylesheets/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
<header id="main-header">
        <!-- boton para el menú lateral -->
      <div id="logo-header" href="principal_arqui.html">
        <nav>
          <ul>
            <li><span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span></li>
          </ul>
        </nav>
      </div> 
        <!-- logo -->
      <a id="logo-header" href="principal_arqui.html">
        <nav>
          <ul>
            <li><img src="images/Image_1.png" alt="" width="130px" height="65px" top="40px"></li>
          </ul>
        </nav>
      </a>
       <!-- / nav -->
      <nav>
        <ul>
          <li><a href="index.html">Cerrar Sesión</a></li>
        </ul>
      </nav>
    
	</header><!-- / #main-header -->

  <!-- Menú lateral -->
  <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a  href="Clientes_Arq.php"><i class="far fa-address-book"></i> Clientes</a>
    <a class="button" href="Proyectos_Arq.php"><i class="far fa-building"></i> Proyectos</a>
      <a  href="Proveedores_Arq.php"><i class="fas fa-donate"></i> Prestadores</a>
        <a  href="Caja_Arq.php"><i class="fas fa-cash-register"></i> Gastos</a>
        <a  href="Cotizaciones_Arq.php"><i class="fas fa-calculator"></i> Cotizaciones</a> 
</div>
  <!-- opciones-->
  <div class="topnav">
    <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop" href="staticBackdrop"><i class="fas fa-users"></i><span> Agregar una cotización</span></button>
    <!-- Topbar Search-->
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
    <form action="proyectoempleados_arq.php">
</form>
      <div class="input-group">
      <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Buscar..">
      </div>
    </form>
  </div>
  <!-- Contenido-->
  <section id="main-content">
  <article>
        <div class="content">
            <?php  
            require('conection.php');
           $output = '';  
           $sql = "SELECT idCotizacion, NombreProy, FechaExpedicion from cotizacion INNER join proyecto on cotizacion.Proyecto= proyecto.idProyecto";  
           $result = mysqli_query($link, $sql);   
               echo '<div class="table-responsive">  
                     <table  id="myTable" class="table table-striped table-hover">  
                      <thead>
                          <tr>
                                <th width="25%">ID</th>    
                               <th width="25%">Nombre</th>  
                               <th width="25%">Feha de expedicion</th>  
                          </tr>
                          </thead>';
                          echo "<tbody>";
                           while($row = mysqli_fetch_assoc($result)){
                          echo "<tr>";
                                echo "<td>".$row["idCotizacion"]."</td>";
                              echo "<td>".$row["NombreProy"]."</td>";
                              echo "<td>".$row["FechaExpedicion"]."</td>";
                              echo "<td><a href='./servicios/agregar_cotiser.php?id=".$row['idCotizacion']."' class='btn btn-primary'>Agregar servicios</a>";
                          echo "</tr>";
                      }
                  echo "</tbody>";
              echo "</table>";
              echo "</div>"  
                          ?> 
          </div>
    </article>
    <!-- /article-->
  </section>
  <!-- Modal-->
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="POST" action="./servicios/insert_cotizacion_arq.php">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar nuevo usuario</h5>
            <button class="btn btn-outline-danger" type="button" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
          </div>
          <div class="modal-body">
          <div class="row g-3">
              <div class="col-md-12">
                <label class="form-label" for="inputNomProy">Proyecto de la cotizacion</label>
                <input type="text" class="form-control" list="proy" name="proy" required>
                  <datalist id="proy">
                      <?php
                    require('./conection.php');
                      $query="SELECT idProyecto, NombreProy FROM proyecto";
                      $result=mysqli_query($link,$query);
                      while($row = mysqli_fetch_assoc($result)){
                          echo "<option value=".$row["idProyecto"].">".$row["NombreProy"]."</option>";
                              }
                    ?>
                  </datalist>
              </div>
            </div>
            <br>
            <div class="modal-footer"><img src="images/logo-consultarq.png" alt="" width="148px" height="20px" top="40px">
              <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Cerrar</button>
              <button class="btn btn-primary" type="submit" action="register">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  
  </div>
<!-- animación barra lateral -->
<script>
  function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    document.getElementById("main-content").style.backgroundColor = "rgba(0,0,0,0.08)";
    /*document.getElementById("main-content").style.marginLeft = "250px"; */
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "white";
    document.getElementById("main-content").style.backgroundColor = "white";
    /*document.getElementById("main-content").style.marginLeft= "0";*/
  }
  </script>
   <script>
    function myFunction() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
    
      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
    </script>
</body>

</html>