<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="stylesheets/estilos_clientes_adm.css" type="text/css">
  <link rel="stylesheet" href="stylesheets/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<?php  
      require('../conection.php');
      $id = $_GET['id'];
     $output = '';  
     $sql = "SELECT * FROM servicios WHERE idServicios = $id";  
     $result = mysqli_query($link, $sql);   
     $row = mysqli_fetch_assoc($result);
    echo '<div class="modal-dialog">
    <div class="modal-content">
      <form method="POST" action="update_servicio.php">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modificar servicio</h5>
        </div>
        <div class="modal-body">
          <div class="row g-3">
            <div class="col">
              <label class="form-label" for="inputArqPrin">Nombre del servicio</label>
              <input class="form-control" id="inputArqPrin" type="text" name="nombreServicio"  value="'.$row["NombreServicio"].'" required>
              <input class="form-control" name="id" type="hidden" value='.$id.'>
              </div>
          </div>
          <div class="row g-3">
            <div class="col">
              <label class="form-label" for="inputArqPrin">Descripción del servicio</label><br>
              <input class="form-control" id="inputArqPrin" type="text" name="descServicio" value="'.$row["DescripcionServico"].'" required>
            </div>
          </div>
          <div class="row g-3">
          <div class="col">
              <label class="form-label" for="inputArqPrin">Precio unitario</label><br>
              <input class="form-control" id="inputArqPrin" type="number" name="preUnitario" value="'.$row["PrecioUnitario"].'" required>
            </div>
            </div>
            <div class="row g-3">
            <div class="col-md-12">
              <label class="form-label" for="inputNomProy">Area de servicio</label>
              <input type="text" class="form-control" list="servicios" name="servicios" required>
                <datalist id="servicios">';?>
                    <?php
                  require('../conection.php');
                    $query="SELECT * FROM area";
                    $result=mysqli_query($link,$query);
                    while($row = mysqli_fetch_assoc($result)){
                        echo "<option value=".$row["idArea"].">".$row["NombreArea"]."</option>";
                            }
                  ?>
                   <?php
              echo '</datalist>
            </div>
          </div>
          </div>
          <br>
          <div class="modal-footer"><img src="../images/logo-consultarq.png" alt="" width="148px" height="20px" top="40px">
          <a href="../servicios.php" class="btn btn-secondary" type="button" data-bs-dismiss="modal">Cancelar</a>
            <button class="btn btn-primary" type="submit" action="register">Guardar</button>
          </div>
        </div>
      </form>
    </div>
  </div>';
      ?> 
</body>
</html> 