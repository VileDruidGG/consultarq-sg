<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../stylesheets/estilos_clientes_adm.css" type="text/css">
  <link rel="stylesheet" href="../stylesheets/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
<header id="main-header">
        <!-- boton para el menú lateral -->
      <div id="logo-header" href="principal_admi.html">
        <nav>
          <ul>
            <li><span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span></li>
          </ul>
        </nav>
      </div> 
        <!-- logo -->
      <a id="logo-header" href="principal_admi.html">
        <nav>
          <ul>
            <li><img src="../images/Image_1.png" alt="" width="130px" height="65px" top="40px"></li>
          </ul>
        </nav>
      </a>
       <!-- / nav -->
      <nav>
        <ul>
          <li><a href="index.html">Cerrar Sesión</a></li>
        </ul>
      </nav>
    
	</header><!-- / #main-header -->

  <!-- Menú lateral -->
  <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="../Clientes_Adm.php"><i class="far fa-address-book"></i> Clientes</a>
      <a  href="../Proyectos_Adm.php"><i class="far fa-building"></i> Proyectos</a>
        <a  href="../Proveedores_Adm.php"><i class="fas fa-donate"></i> Prestadores</a>
          <a  href="../Caja_Adm.php"><i class="fas fa-cash-register"></i> Gastos</a>
          <a href="../Cotizaciones_Adm.php"><i class="fas fa-calculator"></i> Cotizaciones</a>
          <a class="button" href="../Usuarios_Adm.php"><i class="far fa-user"></i> Usuarios</a>
          <a href="../servicios.php"><i class="fas fa-briefcase"></i> Servicios</a>
          <a href="../area.php"><i class="far fa-building"></i> Areas</a>
          <a  href="../Empleados_Adm.php"><i class="far fa-address-card"></i> Empleados</a>
          <a href="../tipoprov.php"><i class="far fa-user-circle"></i> Tipo de Provedores</a>
          <a href="../especialidad.php"><i class="fas fa-people-arrows"></i> Especialidad</a>
</div>
  <!-- opciones-->
  <div class="topnav">
    <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop" href="staticBackdrop"><i class="fas fa-users"></i><span> Agregar Servicios</span></button>
    <!-- Topbar Search-->
  </div>
  <!-- Contenido-->
  <section id="main-content">
    <article>
        <div class="content">
            <?php  
            require('../conection.php');
            $id = $_GET['id'];
           $output = '';  
           $sql = "SELECT idCotiser, NombreServicio, Unidades, PrecioTotal from cotizacion_has_servicios INNER JOIN servicios on cotizacion_has_servicios.idServicios=servicios.idServicios where idCotizacion=$id";  
           $result = mysqli_query($link, $sql);   
               echo '<div class="table-responsive">  
                     <table  id="myTable" class="table table-striped table-hover">  
                      <thead>
                          <tr>
                                <th width="25%">ID</th>    
                               <th width="25%">Nombre del servicio</th>  
                               <th width="25%">Unidades</th>  
                               <th width="25%">PrecioTotal</th>  
                          </tr>
                          </thead>';
                          echo "<tbody>";
                           while($row = mysqli_fetch_assoc($result)){
                          echo "<tr>";
                                echo "<td>".$row["idCotiser"]."</td>";
                              echo "<td>".$row["NombreServicio"]."</td>";
                              echo "<td>".$row["Unidades"]."</td>";
                              echo "<td>".$row["PrecioTotal"]."</td>";
                              echo "<td><a href='modificar_cotiser.php?id=".$row['idCotiser']."' class='btn btn-primary'>Modificar servicios</a>";
                              echo "<td><a href='delete_cotiser.php?id=".$row['idCotiser']."' class='btn btn-danger'>Eliminar</a>";
                          echo "</tr>";
                      }
                  echo "</tbody>";
              echo "</table>";
              echo "</div>"  ;
              $query= "SELECT SUM(PrecioTotal) from cotizacion_has_servicios where idCotizacion=$id";
              $result=mysqli_query($link,$query);
              while ($row = $result->fetch_assoc()) {
               echo "<p><span>Gastos Totales:</span>".$row['SUM(PrecioTotal)']."</p>";
              }
                          ?> 
          </div>
    </article>
    <!-- /article-->
  </section>
  <!-- Modal-->
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="POST" action="insert_cotiser.php">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar nuevo servicio a la cotizacion</h5>
            <button class="btn btn-outline-danger" type="button" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
          </div>
          <div class="modal-body">
          <div class="row g-3">
              <div class="col">
                <label class="form-label" for="inputArqPrin">Unidades</label>
                <input class="form-control" id="inputArqPrin" type="text" name="unidades" required>
                <?php  
                 $id = $_GET['id'];
                echo '<input class="form-control" name="id" type="hidden" value='.$id.'>'
                ?> 
              </div>
            </div>
            <div class="row g-3">
              <div class="col">
                <label class="form-label" for="inputArqPrin">Precio Total</label><br>
                <input class="form-control" id="inputArqPrin" type="number" name="precio" required> 
              </div>
            </div>
            <div class="row g-3">
              <div class="col-md-12">
                <label class="form-label" for="inputNomProy">Servicio</label>
                <input type="text" class="form-control" list="serv" name="serv" required>
                  <datalist id="serv">
                      <?php
                    require('../conection.php');
                      $query="SELECT idServicios, NombreServicio, PrecioUnitario FROM servicios";
                      $result=mysqli_query($link,$query);
                      while($row = mysqli_fetch_assoc($result)){
                          echo "<option value=".$row["idServicios"].">".$row["NombreServicio"]." $".$row["PrecioUnitario"]."</option>";
                              }
                    ?>
                  </datalist>
              </div>
            </div>
            <br>
            <div class="modal-footer"><img src="../images/logo-consultarq.png" alt="" width="148px" height="20px" top="40px">
              <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Cerrar</button>
              <button class="btn btn-primary" type="submit" action="register">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  
  </div>
<!-- animación barra lateral -->
<script>
  function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    document.getElementById("main-content").style.backgroundColor = "rgba(0,0,0,0.08)";
    /*document.getElementById("main-content").style.marginLeft = "250px"; */
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "white";
    document.getElementById("main-content").style.backgroundColor = "white";
    /*document.getElementById("main-content").style.marginLeft= "0";*/
  }
  </script>
   <script>
    function myFunction() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
    
      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
    </script>
</body>

</html>