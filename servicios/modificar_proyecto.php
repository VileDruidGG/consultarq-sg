<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="stylesheets/estilos_clientes_adm.css" type="text/css">
  <link rel="stylesheet" href="stylesheets/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<?php  
      require('../conection.php');
      $id = $_GET['id'];
     $output = '';  
     $sql = "SELECT * FROM proyecto WHERE idProyecto = $id";  
     $result = mysqli_query($link, $sql);   
     $row = mysqli_fetch_assoc($result);
    echo '<div class="modal-dialog">
    <div class="modal-content">
      <form method="POST" action="update_proyecto.php">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar nuevo proyecto</h5>
        </div>
        <div class="modal-body">
          <div class="row g-3">
            
              <label class="form-label" for="inputArqPrin">Nombre del proyecto</label>
              <input class="form-control" id="inputArqPrin" type="text" name="nombre" value="'.$row["NombreProy"].'">
              <input class="form-control" name="id" type="hidden" value='.$id.'>
              <div class="col-md-12">
              <label class="form-label" for="inputArqPrin">Descripcion del proyecto</label><br>
              <textarea class="form-control" id="exampleMessage" name="descrip" >'.$row["DescripcionProy"].'</textarea>
            </div>
           
            <div class="col">
              <label class="form-label" for="inputArqPrin">Area del proyecto</label>
              <input type="text" class="form-control" list="area" name="area">
                <datalist id="area">';?>
                    <?php
                  require('../conection.php');
                    $query="SELECT idArea, NombreArea FROM area";
                    $result=mysqli_query($link,$query);
                    while($row = mysqli_fetch_assoc($result)){
                        echo "<option value=".$row["idArea"].">".$row["NombreArea"]."</option>";
                            }
                  ?>
                  <?php
                echo '</datalist>
            </div>
          </div>
          <div class="row g-3">
            <div class="col-md-12">
              <label class="form-label" for="inputNomProy">Cliente</label>
              <input type="text" class="form-control" list="cliente" name="cliente">
                <datalist id="cliente">';?>
                    <?php
                  require('../conection.php');
                    $query="SELECT idCliente, Nombre FROM cliente";
                    $result=mysqli_query($link,$query);
                    while($row = mysqli_fetch_assoc($result)){
                        echo "<option value=".$row["idCliente"].">".$row["Nombre"]."</option>";
                            }
                  ?>
                  <?php
                echo '</datalist>
            </div>
          </div>
          <div class="row g-3">
            
          </div>
          <br>
          <div class="modal-footer"><img src="../images/logo-consultarq.png" alt="" width="148px" height="20px" top="40px">
          <a href="../Proyectos_Adm.php" class="btn btn-secondary" type="button" data-bs-dismiss="modal">Cancelar</a>
            <button class="btn btn-primary" type="submit" action="register">Guardar</button>
          </div>
        </div>
      </form>
    </div>
  </div>';
      ?> 
</body>
</html> 