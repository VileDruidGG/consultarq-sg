<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../stylesheets/estilos_clientes_adm.css" type="text/css">
  <link rel="stylesheet" href="../stylesheets/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<header id="main-header">
        <!-- boton para el menú lateral -->
      <div id="logo-header" href="principal_arqui.html">
        <nav>
          <ul>
            <li><span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span></li>
          </ul>
        </nav>
      </div> 
        <!-- logo -->
      <a id="logo-header" href="principal_arqui.html">
        <nav>
          <ul>
            <li><img src="../images/Image_1.png" alt="" width="130px" height="65px" top="40px"></li>
          </ul>
        </nav>
      </a>
       <!-- / nav -->
      <nav>
        <ul>
          <li><a href="index.html">Cerrar Sesión</a></li>
        </ul>
      </nav>
    
	</header><!-- / #main-header -->

  <!-- Menú lateral -->
  <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a  href="Clientes_Arq.php"><i class="far fa-address-book"></i> Clientes</a>
    <a class="button" href="../Proyectos_Arq.php"><i class="far fa-building"></i> Proyectos</a>
      <a  href="../Proveedores_Arq.php"><i class="fas fa-donate"></i> Prestadores</a>
        <a  href="../Caja_Arq.php"><i class="fas fa-cash-register"></i> Gastos</a>
        <a  href="../Cotizaciones_Arq.php"><i class="fas fa-calculator"></i> Cotizaciones</a> 
</div>
  <!-- opciones-->
  <div class="topnav">
  <a href="../Proyectos_Arq.php" class="btn btn-primary" type="button" data-bs-dismiss="modal">Regresar</a>
  </div>
  <!-- Contenido-->
  <section id="main-content">
  <article>
        <div class="content">
            <?php  
            require('../conection.php');
            $id = $_GET['id'];
           $output = '';  
           $sql = "SELECT id_Caja, Concepto, Tipo, FechayHora, Folio, NombreProy, Monto FROM caja INNER JOIN proyecto on caja.Proyecto=proyecto.idProyecto where proyecto= $id";  
           $result = mysqli_query($link, $sql);   
               echo '<div class="table-responsive">  
                     <table  id="myTable" class="table table-striped table-hover">  
                      <thead>
                          <tr>
                                <th width="14%">ID</th>    
                               <th width="14%">Concepto</th>  
                               <th width="14%">Tipo</th>  
                               <th width="14%">Fecha y hora</th>  
                               <th width="14%">Folio</th>  
                               <th width="14%">Proyecto</th> 
                               <th width="14%">Monto</th>  
                          </tr>
                          </thead>';
                          echo "<tbody>";
                           while($row = mysqli_fetch_assoc($result)){
                          echo "<tr>";
                                echo "<td>".$row["id_Caja"]."</td>";
                              echo "<td>".$row["Concepto"]."</td>";
                              echo "<td>".$row["Tipo"]."</td>";
                              echo "<td>".$row["FechayHora"]."</td>";
                              echo "<td>".$row["Folio"]."</td>";
                              echo "<td>".$row["NombreProy"]."</td>";
                              echo "<td>".$row["Monto"]."</td>";
                          echo "</tr>";
                      }
                  echo "</tbody>";
              echo "</table>";
              echo "</div>"  ;
              $query= "SELECT SUM(monto) from caja where tipo='Gasto' and proyecto= $id";
              $result=mysqli_query($link,$query);
              while ($row = $result->fetch_assoc()) {
               echo "<p><span>Gastos Totales:</span>".$row['SUM(monto)']."</p>";
              }
                $query= "SELECT SUM(monto) from caja where tipo='Ingreso' and proyecto= $id";
              $result=mysqli_query($link,$query);
              while ($row = $result->fetch_assoc()) {
               echo "<p><span>Ingresos Totales:</span>".$row['SUM(monto)']."</p>";
              }
              
              $link->close();  
                          ?> 
          </div>
    </article>
    <script>
  function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    document.getElementById("main-content").style.backgroundColor = "rgba(0,0,0,0.08)";
    /*document.getElementById("main-content").style.marginLeft = "250px"; */
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "white";
    document.getElementById("main-content").style.backgroundColor = "white";
    /*document.getElementById("main-content").style.marginLeft= "0";*/
  }
  </script>
   <script>
    function myFunction() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
    
      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
    </script>
</body>
</html> 