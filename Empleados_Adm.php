<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="stylesheets/estilos_clientes_adm.css" type="text/css">
  <link rel="stylesheet" href="stylesheets/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="charts/Chart.min.js"></script>
  <script src="charts/utils.js"></script>
</head>

<body>
  <header id="main-header">
        <!-- boton para el menú lateral -->
      <div id="logo-header" href="principal_admi.html">
        <nav>
          <ul>
            <li><span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span></li>
          </ul>
        </nav>
      </div> 
        <!-- logo -->
      <a id="logo-header" href="principal_admi.html">
        <nav>
          <ul>
            <li><img src="images/Image_1.png" alt="" width="130px" height="65px" top="40px"></li>
          </ul>
        </nav>
      </a>
       <!-- / nav -->
      <nav>
        <ul>
          <li><a href="index.html">Cerrar Sesión</a></li>
        </ul>
      </nav>
  </header>
  <!-- / #main-header-->
  <!-- Menú lateral-->
  <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="Clientes_Adm.php"><i class="far fa-address-book"></i> Clientes</a>
      <a  href="Proyectos_Adm.php"><i class="far fa-building"></i> Proyectos</a>
        <a  href="Proveedores_Adm.php"><i class="fas fa-donate"></i> Prestadores</a>
          <a  href="Caja_Adm.php"><i class="fas fa-cash-register"></i> Gastos</a>
          <a href="Cotizaciones_Adm.php"><i class="fas fa-calculator"></i> Cotizaciones</a>
          <a href="Usuarios_Adm.php"><i class="far fa-user"></i> Usuarios</a>
          <a href="servicios.php"><i class="fas fa-briefcase"></i> Servicios</a>
          <a href="area.php"><i class="far fa-building"></i> Areas</a>
          <a class="button" href="Empleados_Adm.php"><i class="far fa-address-card"></i> Empleados</a>
          <a href="tipoprov.php"><i class="far fa-user-circle"></i> Tipo de Provedores</a>
          <a href="especialidad.php"><i class="fas fa-people-arrows"></i> Especialidad</a>
</div>
  <!-- opciones-->
  
  <div class="topnav">
    <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop" href="staticBackdrop"><i class="fas fa-users"></i><span> Agregar Empleado</span></button>
    <!-- Topbar Search-->
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
      <div class="input-group">
      <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Buscar..">
      </div>
    </form>
  </div>
  <!-- Contenido-->
  <section id="main-content">
    <article>
        <div class="content">
            <?php  
            require('conection.php');
           $output = '';  
           $sql = "SELECT id_Empleado, Nombre, Apellidos, Telefono, Sueldo, Calle, Colonia, Numero, CP, especialidad.NombreEsp from empleado INNER JOIN especialidad on empleado.Especialidad=especialidad.idEspecialidad";  
           $result = mysqli_query($link, $sql);   
               echo '<div class="table-responsive">  
                     <table  id="myTable" class="table table-striped table-hover">  
                      <thead>
                          <tr>
                                <th width="14%">ID</th>    
                               <th width="14%">Nombre</th>  
                               <th width="14%">Apellidos</th>  
                               <th width="14%">Telefono</th>  
                               <th width="14%">Sueldo</th> 
                               <th width="14%">Calle</th>
                               <th width="14%">Colonia</th>
                               <th width="14%">Numero</th>
                               <th width="14%">Codigo Postal</th>
                               <th width="14%">Especialidad</th>    
                          </tr>
                          </thead>';
                          echo "<tbody>";
                           while($row = mysqli_fetch_assoc($result)){
                          echo "<tr>";
                                echo "<td>".$row["id_Empleado"]."</td>";
                              echo "<td>".$row["Nombre"]."</td>";
                              echo "<td>".$row["Apellidos"]."</td>";
                              echo "<td>".$row["Telefono"]."</td>";
                              echo "<td>".$row["Sueldo"]."</td>";
                              echo "<td>".$row["Calle"]."</td>";
                              echo "<td>".$row["Colonia"]."</td>";
                              echo "<td>".$row["Numero"]."</td>";
                              echo "<td>".$row["CP"]."</td>";
                              echo "<td>".$row["NombreEsp"]."</td>";
                              echo "<td><a href='./servicios/modificar_empleado.php?id=".$row['id_Empleado']."' class='btn btn-primary'>Modificar</a>";
                              echo "<td><a href='./servicios/delete_empleado.php?id=".$row['id_Empleado']."' class='btn btn-danger'>Eliminar</a>";
                          echo "</tr>";
                      }
                  echo "</tbody>";
              echo "</table>";
              echo "</div>"  
                          ?> 
          </div>
    </article>
    <!-- /article-->
  </section>
  <!-- Modal-->
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="POST" action="./servicios/insert_empleado.php">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar nuevo usuario</h5>
            <button class="btn btn-outline-danger" type="button" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
          </div>
          <div class="modal-body">
            <div class="row g-3">
              <div class="col">
                <label class="form-label" for="inputArqPrin">Nombre del Arquitecto</label>
                <input class="form-control" id="inputArqPrin" type="text" name="nombre" required>
              </div>
              <div class="col">
                <label class="form-label" for="inputArqSec">Apellidos del Arquitecto</label>
                <input class="form-control" id="inputArqSec" type="text" name="apellidos" required>
              </div>
            </div>
            <div class="row g-3">
              <div class="col-md-12">
                <label class="form-label" for="inputNomProy">Telefono</label>
                <input class="form-control" id="inputNomProy" type="number" name="telefono" required>
              </div>
              <div class="col-md-12">
                <label class="form-label" for="inputCliente">Sueldo</label>
                <input class="form-control" id="inputCliente" type="number" name="sueldo" required>
              </div>
            </div>
            <div class="row g-3">
              <div class="col">
                <label class="form-label" for="inputArqPrin">Calle</label>
                <input class="form-control" id="inputArqPrin" type="text" name="calle" required>
              </div>
              <div class="col">
                <label class="form-label" for="inputArqSec">Colonia</label>
                <input class="form-control" id="inputArqSec" type="text" name="colonia" required>
              </div>
            </div>
            <div class="row g-3">
              <div class="col">
                <label class="form-label" for="inputArqPrin">Numero</label>
                <input class="form-control" id="inputArqPrin" type="number" name="numero" required>
              </div>
              <div class="col">
                <label class="form-label" for="inputArqSec">Código Postal</label>
                <input class="form-control" id="inputArqSec" type="number" name="codigo_postal" required>
              </div>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label>Especialidad</label>
                <input type="text" class="form-control" list="esp" name="esp" required> 
                  <datalist id="esp">
                      <?php
                    require('./conection.php');
                      $query="SELECT * FROM especialidad";
                      $result=mysqli_query($link,$query);
                      while($row = mysqli_fetch_assoc($result)){
                          echo "<option value=".$row["idEspecialidad"].">".$row["NombreEsp"]."</option>";
                              }
                    ?>
                  </datalist>
              </div>      
            <div class="modal-footer"><img src="images/logo-consultarq.png" alt="" width="148px" height="20px" top="40px">
              <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Cerrar</button>
              <button class="btn btn-primary" type="submit" action="register">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>

  <script>
  function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    document.getElementById("main-content").style.backgroundColor = "rgba(0,0,0,0.08)";
    /*document.getElementById("main-content").style.marginLeft = "250px"; */
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "white";
    document.getElementById("main-content").style.backgroundColor = "white";
    /*document.getElementById("main-content").style.marginLeft= "0";*/
  }
  </script>
   <script>
    function myFunction() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
    
      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
    </script>
</body>

</html>